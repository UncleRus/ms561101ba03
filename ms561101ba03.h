/*
 * Driver for barometic pressure sensor ms511-01BA03
 *
 * Copyright (C) 2016 Bernhard Guillon <Bernhard.Guillon@begu.org>
 *
 * Loosely based on hmc5831 with:
 * Copyright (C) 2016 Ruslan V. Uss <unclerus@gmail.com>
 * BSD Licensed as described in the file LICENSE
 */
#ifndef EXTRAS_MS561101BA03_H_
#define EXTRAS_MS561101BA03_H_

#include <stdint.h>
#include <stdbool.h>

#ifdef __cplusplus
extern "C"
{
#endif

typedef struct
{
    uint16_t sens;       // C1 Pressure sensitivity                             | SENS_t1
    uint16_t off;        // C2 Pressure offset                                  | OFF_t1
    uint16_t tcs;        // C3 Temperature coefficient of pressure sensitivity  | TCS
    uint16_t tco;        // C4 Temperature coefficient of pressuer offset       | TCO
    uint16_t t_ref;      // C5 Reference temperature                            | T_ref
    uint16_t tempsens;   // C6 Temperature coefficient of the temperature       | TEMPSENSE
} ms561101ba03_config_data_t;

typedef enum
{
    _256  = 0x00,
    _512  = 0x02,
    _1024 = 0x04,
    _2048 = 0x06,
    _4096 = 0x08
} ms561101ba03_osr_t;

int32_t ms561101ba03_get_temperature( ms561101ba03_config_data_t * config_data, ms561101ba03_osr_t osr );
int32_t ms561101ba03_get_pressure( ms561101ba03_config_data_t * config_data, ms561101ba03_osr_t osr );

bool ms561101ba03_init(ms561101ba03_config_data_t * config_data);

#ifdef __cplusplus
}
#endif
#endif /* EXTRAS_MS561101BA03_H_ */
