/*
 * Driver for barometic pressure sensor ms511-01BA03
 *
 * Copyright (C) 2016 Bernhard Guillon <Bernhard.Guillon@web.de>
 *
 * Loosely based on hmc5831 with:
 * Copyright (C) 2016 Ruslan V. Uss <unclerus@gmail.com>
 * BSD Licensed as described in the file LICENSE
 */
#include "ms561101ba03.h"
#include <i2c/i2c.h>
#include <espressif/esp_common.h>
#include "FreeRTOS.h"
#include "task.h"

// Register configuration see datasheet
#define ADDRESS_CSB_HIGH    0x76
#define ADDRESS_CSB_LOW     0x77

#define CONVERT_D1          0x40
#define CONVERT_D2          0x50
#define ADC_READ            0x00
#define CONVERSION_TIME     20   // microseconds
#define ADDR                ADDRESS_CSB_LOW // FIXME: make this configurable at init
static const uint8_t RESET = 0x1E;

static inline void reset()
{
     uint8_t buf[1] = { RESET };
     i2c_slave_write(ADDR, buf, 1);
}

static inline ms561101ba03_config_data_t read_prom()
{
    ms561101ba03_config_data_t config = {0,0,0,0,0,0};

    uint8_t tmp[2] = {0,0};
    i2c_slave_read(ADDR, 0xA2, tmp, 2);
    config.sens = tmp[0] << 8 | tmp[1];

    i2c_slave_read(ADDR, 0xA4, tmp, 2);
    config.off = tmp[0] << 8 | tmp[1];

    i2c_slave_read(ADDR, 0xA6, tmp, 2);
    config.tcs = tmp[0] << 8 | tmp[1];

    i2c_slave_read(ADDR, 0xA8, tmp, 2);
    config.tco = tmp[0] << 8 | tmp[1];

    i2c_slave_read(ADDR, 0xAA, tmp, 2);
    config.t_ref = tmp[0] << 8 | tmp[1];

    i2c_slave_read(ADDR, 0xAB, tmp, 2);
    config.tempsens = tmp[0] << 8 | tmp[1];

    return config;
}

static inline void start_pressure_conversion( ms561101ba03_osr_t osr ) { //D1
     uint8_t buf[1] = { CONVERT_D1 + osr};
     i2c_slave_write(ADDR, buf, 1);
}

static inline void start_temperature_conversion( ms561101ba03_osr_t osr ) { //D2
     uint8_t buf[1] = { CONVERT_D2 + osr};
     i2c_slave_write(ADDR, buf, 1);
}

static inline uint32_t read_adc() {
    uint32_t ret = 0;
    uint8_t tmp[3] = {0,0,0};
    i2c_slave_read(ADDR, 0x00, tmp, 3);
    ret = tmp[0] << 16 | tmp[1] << 8 | tmp[0];
    return ret;
}

static inline int32_t calc_dt( uint32_t digital_temperature, uint16_t t_ref ) {
    // Difference between actual and reference digital_temperature
    // dT = D2 - T_ref = D2 - C5 *2^8
    int32_t dt =0;
    dt = digital_temperature - (t_ref << 8);
    return dt;
}

static inline int32_t calc_temp( uint32_t dT, uint16_t tempsens ) {
    // Actual temperatuer (-40...85C with 0.01 resulution)
    // TEMP = 20C +dT * TEMPSENSE =2000 + dT * C6 / 2^23
    int32_t temp =0;
    temp = 2000 +(dT * tempsens / 8388608);
    return temp;
}

static inline int64_t calc_off( ms561101ba03_config_data_t * config_data, int32_t dT )
{
    // Offset at actual temperature
    // OFF=OFF_t1 + TCO * dT = OFF_t1(C2) * 2^16 + (C4*dT)/2^7
    return (((int64_t)config_data->off) << 16) + ((config_data->tco * dT) >> 7);
}

static inline int64_t calc_sens( ms561101ba03_config_data_t * config_data, int32_t dT )
{
    // Senisitivity at actual temperature
    // SENS=SENS_t1 + TCS *dT = SENS_t1(C1) *2^15 + (TCS(C3) *dT)/2^8
    return (((int64_t)config_data->sens) << 15) + (( config_data->tcs * dT) >> 8);;
}

static inline int32_t calc_p( uint32_t digital_pressure, int64_t sens, int64_t off)
{
    // Temperature compensated pressure (10...1200mbar with 0.01mbar resolution
    // P = digital pressure value  * SENS - OFF = (D1 * SENS/2^21 -OFF)/2^15
    return  (((( digital_pressure * sens) >> 21) - off) >> 15);;
}

static inline int32_t get_temperature( ms561101ba03_config_data_t * config_data, ms561101ba03_osr_t osr, int32_t * dT )
{
    start_temperature_conversion(_4096);
    vTaskDelay(CONVERSION_TIME / portTICK_PERIOD_MS);
    uint32_t dig_temp = read_adc();
    *dT = calc_dt(dig_temp, config_data->t_ref);
    return calc_temp(*dT, config_data->tempsens);
}

/////////////////////////Public//////////////////////////////////////

int32_t ms561101ba03_get_temperature( ms561101ba03_config_data_t * config_data, ms561101ba03_osr_t osr )
{
    int32_t dT = 0;
    return get_temperature( config_data, osr, &dT );
}

int32_t ms561101ba03_get_pressure( ms561101ba03_config_data_t * config_data, ms561101ba03_osr_t osr )
{
    // Second order temperature compensation
    int32_t dT = 0;
    int32_t temp = get_temperature( config_data, osr, &dT );
    printf("GET PRESSURE WITH temp %i ", temp);
    start_temperature_conversion(_4096);
    vTaskDelay(CONVERSION_TIME / portTICK_PERIOD_MS);
    uint32_t dig_pressure = read_adc();
    int64_t off  = calc_off(config_data, dT);
    int64_t sens = calc_sens(config_data, dT);
    int32_t p = calc_p(dig_pressure, sens, off);//(((( dig_press * sens) >> 21) - off) >> 15);
    return p;
}

bool ms561101ba03_init(ms561101ba03_config_data_t * config_data)
{
    // First of all we need to reset the chip 
    reset();
    // Wait a bit for the device to reset
    vTaskDelay(CONVERSION_TIME / portTICK_PERIOD_MS);
    // Get the config
    *config_data = read_prom();
    // And return
    return true; //FIXME: check config and check i2c errors!
}
